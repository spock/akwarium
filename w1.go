package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

const (
	// W1_DEVLIST - Path to file with list of 1-wire connected devices
	W1_DEVLIST string = "/sys/devices/w1_bus_master1/w1_master_slaves"
	//W1_SLAVE_PATH  - Path with temp sensor data
	W1_SLAVE_PATH string = "/sys/devices/w1_bus_master1/%s/w1_slave"
)

// GetTemp - process temp sensor data and return temp is Celsius degrees
func GetTemp(sensor string) (temp float64, err error) {
	//var temp float32 = 0
	f, err := ioutil.ReadFile(fmt.Sprintf(W1_SLAVE_PATH, sensor))
	if err != nil {
		return 0, err
	}
	w1Data := strings.Split(string(f), "\n")
	//if len(w1Data) == 2 {
	m := regexp.MustCompile("(t=)(\\d+$)")
	if strings.Contains(w1Data[0], "YES") {
		temps := m.FindAllString(w1Data[1], -1)
		if len(temps) > 0 {
			t := strings.Split(temps[0], "=")
			temp, _ = strconv.ParseFloat(t[1], 32)
		}
		return temp / 1000, nil
	}
	return temp, fmt.Errorf("Wrong data readed from sensor %s\n", sensor)
}

// List1WBus - list 1-wire bus and return array of all connected devices
func List1WBus() ([]string, error) {
	var result []string
	var err error
	fcontent, err := ioutil.ReadFile(W1_DEVLIST)
	if err != nil {
		return result, err
	}
	result = strings.Split(string(fcontent), "\n")
	return result, nil

}
func main() {
	slaves, err := List1WBus()
	if err != nil {
		log.Fatal("Unable to get W1 device list", err)
		os.Exit(-1)
	}
	for _, dev := range slaves {
		if len(dev) > 2 {
			r, err := GetTemp(dev)
			if err != nil {
				log.Fatal(err)
				continue
			} else {
				fmt.Printf("%s:%v\n", dev, r)
			}
		}
	}
}
